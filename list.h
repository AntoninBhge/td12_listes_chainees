#ifndef LIB_LIST
#define LIB_LIST
#include <stdlib.h>
#include <stdio.h>
#define DELETE(ptr) free(ptr) //Défini DELETE(ptr) comme équivalent à free(ptr).
#define TRUE 1 //Défini TRUE comme équivalent à 1.
#define FALSE 0 //Défini FALSE comme équivalent à 0.
#define GETV(data, type) *(type*)data //Défini GETV(data, type) comme équivalent à *(type*)data.

typedef unsigned int BOOL;
typedef unsigned char BOOLEAN;
typedef struct element_ element; //Typedef pour pouvoir ecrire "element" à la place de "struct element_"
typedef struct list_ list;


struct element_ //Création de la structure element_
{
    void* data;
    element* next; 
    element* previous;
};


struct list_  //Création de la structure list_
{
    element* first;
    element* last;
    unsigned int length;

};

list* createList(); //Prototype de la fonction qui permet de créer une liste.

void purgeAllList(list* self); //Permet de supprimer les éléments de la liste.

element* createElement(void* data, element* previous, element* next); //Prototype de la fonction pour ajouter créer un nouvel élement.

void addElement(list* self, void* data); //Prototype de la fonction pour ajouter un nouvel élement à la liste.

void displayList(list* self, int dataType); //Prototype de la fonction qui affiche les élements d'une liste.

void merge(list* list1, list* list2); //Prototype de la fonction qui fusionne 2 listes (la deuxième à la fin de la première).

unsigned int countOccurence(list* self, void* occurence, int dataType); //Prototype de la fonction qui compte les occurences.

void deleteOccurence(list* self, void* occurence, int dataType, BOOL delAll); //Prototype de la fonction qui supprime les occurences.

#endif