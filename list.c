#include "list.h"
#include <stdlib.h>
#include <stdio.h>

list* createList(){ //Fonction qui permet de créer une nouvelle liste.
   list* newList = (list*)malloc(sizeof(list)); 
   newList->first = NULL;
   newList->length = 0;
   return newList;
}

 void purgeAllList(list* self) { //Fonction qui supprime et libère l'espace mémoire de tous les éléments d'une liste.
   if (self->length == 0) return;
   element* current = self->first;
   element* next;
   do {
      next = current->next;
      free(current);
      current = next;
   } while (current != NULL);
   self->length = 0;
   self->first = NULL;
   self->last = NULL;
   printf("La liste a été supprimée.\n");
}

element* createElement(void* data, element* previous, element* next){ //Fonction qui permet de créer un nouvel élément qui sera doublement chainé (previous et next).
   element* newElement = (element*)malloc(sizeof(element));
   newElement->data = data;
   newElement->previous = previous;
   newElement->next = next;
   return newElement;
}

void addElement(list* self, void* data){ //Fonction qui permet d'ajouter un élément à la fin de la liste.
   if (self->length == 0){
      self->first = createElement(data, NULL, NULL); //appelle la fonction createElement car lorsque l'on créé un élément, on souhaite l'ajouter directement à la liste.
      self->last = self->first;
   } else {
      self->last->next = createElement(data, self->last, NULL);
      self->last = self->last->next;
   }
   self->length++; 
}

void displayList(list* self, int dataType){ //Fonction qui permet d'afficher tous les éléments d'une liste en choisissant le type des éléments de la liste.
   printf("La liste contient %d éléments :\n", self->length);
   element* current = self->first;
   int index = 0;
   while(current != NULL){
      if (dataType == 0) printf("élément %d : %d\n", index, *(int*)current->data);
      else if (dataType == 1) printf("élément %d : %f\n", index, *(float*)current->data); 
      else if (dataType == 2) printf("élément %d : %ld\n", index, *(long*)current->data);
      else if (dataType == 3) printf("élément %d : %lf\n", index,*(double*)current->data);
      else if (dataType == 4) printf("élément %d : %s\n", index, (char*)current->data);
      current = current->next;
      index++;
   }
}

void merge(list* list1, list* list2){ //Fonction pour fusionner 2 listes, ajoute la deuxième à la fin de la première et relie leurs pointeurs previous et next entre-eux.
   if(list1->last != NULL) list1->last->next = list2->first; //De plus mets à jours le première élément et le dernier élément.
   if(list2->first != NULL) list2->first->previous = list1->last;
   list1->last = list2->last;
   list1->length+=list2->length;
}

unsigned int countOccurence(list* self, void* occurence, int dataType){ //Fonction qui compte les occurence d'un élément de la liste selon son type.
   element* current = self->first;
   int index = 0;
   while(current != NULL){
      if (dataType == 0 && GETV(current->data, int) == GETV(occurence, int)) index++;
      else if (dataType == 1 && GETV(current->data, float)  == GETV(occurence, float)) index++;
      else if (dataType == 2 && GETV(current->data, long)  == GETV(occurence, long)) index++;
      else if (dataType == 3 && GETV(current->data, double)  == GETV(occurence, double)) index++;
      else if (dataType == 4 && GETV(current->data, char*)  == GETV(occurence, char*)) index++;
      current = current->next;
   }
   return index;
}

void deleteOccurence(list* self, void* occurence, int dataType, BOOL delAll){ //Fonction qui permet de supprimer l'occurence d'un entier.
   element* current = self->first;
   element* tmp = NULL;
   BOOLEAN del = FALSE;
   while (current != NULL){
      switch (dataType) //En fonction du type, cast la variable occurence et si del = 1 alors supprime l'occurence et relie les autres entre-elles.
      {
      case 0:
         if (GETV(current->data, int) == GETV(occurence, int)) del = TRUE;
         break;
      case 1:
         if (GETV(current->data, float) == GETV(occurence, float)) del = TRUE;
         break;
      case 2:
         if (GETV(current->data, long) == GETV(occurence, long)) del = TRUE;
         break;
      case 3:
         if (GETV(current->data, double) == GETV(occurence, double)) del = TRUE;
         break;
      case 4:
         if (GETV(current->data, char*) == GETV(occurence, char*)) del = TRUE;
         break;
      }
      tmp = current; //Déclaration du variable tmp afin de stocker la valeur de la variable current pour ne pas poser de problèmes dans la suite de l'algorithme.
      current = current->next;
      if (del) {
         if (tmp->previous != NULL) tmp->previous->next = tmp->next;
         if (tmp->next != NULL) tmp->next->previous = tmp->previous;
         if (tmp->previous == NULL) self->first = tmp->next;
         if (tmp->next == NULL) self->last = tmp->previous;
         free(tmp);
         self->length--;
         if (!delAll) return; //Si delAll = 0 alors on ne supprime qu'une seule fois l'occurence et on sort du While.
      }
      del = FALSE; //On remet del à 0 afin de ne pas supprimer toutes les éléments de la liste.
   }
      
}