#include "list.h"


int main(int argc, char const *argv[])
{
    int element1 = 1;
    int element2 = 2;
    int element3 = 1;
    int element4 = 1;
    int element5= 5;
    int element6 = 6;
    int eltOccurence = 1;
    
    list* list1 = createList(); //Création des deux listes
    list* list2 = createList();

    //EXERCICE 1
    addElement(list1, &element1); //Ajout et création des éléments aux listes 1 et 2.
    addElement(list1, &element2);
    addElement(list1, &element3);
    addElement(list1, &element4);
    addElement(list2, &element5);
    addElement(list2, &element6);

    //EXERCICE 2
    printf("##### EXERCICE 2 : #####\n");
    displayList(list1, 0); //Affichage des éléments de la liste 1.

    //EXERCICE 3
    printf("##### EXERCICE 3 : #####\n");
    unsigned int nbrOccurence = countOccurence(list1, &eltOccurence, 0); //Compte le nombre d'occurence de nbrOccurence.
    printf("Il y a %d occurence de l'entier %d dans cette liste\n", nbrOccurence, eltOccurence);

    //EXERCICE 4
    printf("##### EXERCICE 4 : #####\n");
    printf("Après suppresion de l'élément %d, ", eltOccurence);
    deleteOccurence(list1, &eltOccurence, 0, TRUE);
    displayList(list1, 0);

    //EXERCICE 5
    printf("##### EXERCICE 5 : #####\n");
    merge(list1, list2); //Fusion des deux listes
    printf("Après la fusion des deux listes, ");
    displayList(list1, 0);
    purgeAllList(list1);
    free(list1); //Libère l'espace mémoire et supprime la liste car la fonction purgeAllList supprime les éléments de la liste mais pas la liste.
    free(list2);
    return 0;
}
 