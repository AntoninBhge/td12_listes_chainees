COMPILER=gcc
FLAGS=-Wall -std=c99
OUTPUT=main.out

all:
	${COMPILER} list.c main.c -o ${OUTPUT} ${FLAGS}