# Important

La question 6 n'as pas été traitée dans une fonction étant donné que je compte les éléments à chaque ajout d'un élément dans une liste. De plus, dès que j'affiche une liste, le nombre d'éléments dans cette liste est affiché.

Toutes les fonctions se trouvent dans le fichier list.c et leurs prototypes dans le list.h

Dans le main.c, les éléments sont affichés en fonction de l'ordre des questions du TD.